import React, { Component } from "react";
import PostDataService from "../services/post";

export default class Post extends Component {
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeContent = this.onChangeContent.bind(this);
    this.onChangeImage = this.onChangeImage.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
    this.getPost = this.getPost.bind(this);
    this.updatePost = this.updatePost.bind(this);
    this.deletePost = this.deletePost.bind(this);

    this.state = {
      currentPost: {
        id: null,
        title: "",
        content: "",
        image: "",
        category: "",
      },
      message: "",
    };
  }

  componentDidMount() {
    this.getPost(this.props.match.params.id);
  }

  onChangeTitle(e) {
    const title = e.target.value;

    this.setState(function (prevState) {
      return {
        currentPost: {
          ...prevState.currentPost,
          title: title,
        },
      };
    });
  }

  onChangeContent(e) {
    const content = e.target.value;

    this.setState((prevState) => ({
      currentPost: {
        ...prevState.currentPost,
        content,
      },
    }));
  }

  onChangeImage(e) {
    const image = e.target.value;

    this.setState((prevState) => ({
      currentPost: {
        ...prevState.currentPost,
        image,
      },
    }));
  }

  onChangeCategory(e) {
    const category = e.target.value;

    this.setState((prevState) => ({
      currentPost: {
        ...prevState.currentPost,
        category,
      },
    }));
  }

  getPost(id) {
    PostDataService.get(id)
      .then((response) => {
        this.setState({
          currentPost: response.data,
        });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  updatePost() {
    PostDataService.update(this.state.currentPost.id, this.state.currentPost)
      .then((response) => {
        console.log(response.data);
        this.setState({
          message: "The post was updated successfully!",
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  deletePost() {
    PostDataService.delete(this.state.currentPost.id)
      .then((response) => {
        console.log(response.data);
        this.props.history.push("/posts");
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentPost } = this.state;

    return (
      <div>
        {currentPost ? (
          <div className="edit-form">
            <h4>Post</h4>
            <form>
              <div className="form-group">
                <label htmlFor="title">Title</label>
                <input
                  type="text"
                  className="form-control"
                  id="title"
                  value={currentPost.title}
                  onChange={this.onChangeTitle}
                />
              </div>
              <div className="form-group">
                <label htmlFor="content">Content</label>
                <textarea
                  type="text"
                  rows="5"
                  className="form-control"
                  id="content"
                  value={currentPost.content}
                  onChange={this.onChangeContent}
                />
              </div>
              <div className="form-group">
                <label htmlFor="image">Image</label>
                <input
                  type="text"
                  className="form-control"
                  id="image"
                  value={currentPost.image}
                  onChange={this.onChangeImage}
                />
              </div>
              <div className="form-group">
                <label htmlFor="category">Category</label>
                <input
                  type="text"
                  className="form-control"
                  id="category"
                  value={currentPost.onChangeCategory}
                />
              </div>
            </form>

            <button
              className="badge badge-danger mr-2"
              onClick={this.deletePost}
            >
              Delete
            </button>

            <button
              type="submit"
              className="badge badge-success"
              onClick={this.updatePost}
            >
              Update
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Post...</p>
          </div>
        )}
      </div>
    );
  }
}
