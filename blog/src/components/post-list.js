import React, { Component } from "react";
import PostDataService from "../services/post";
import { Link } from "react-router-dom";

export default class PostsList extends Component {
  constructor(props) {
    super(props);
    this.retrievePost = this.retrievePost.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActivePost = this.setActivePost.bind(this);

    this.state = {
      posts: [],
      currentPost: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrievePost();
  }

  retrievePost() {
    PostDataService.getAll()
      .then((response) => {
        this.setState({
          posts: response.data,
        });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrievePost();
    this.setState({
      currentPost: null,
      currentIndex: -1,
    });
  }

  setActivePost(post, index) {
    this.setState({
      currentPost: post,
      currentIndex: index,
    });
  }

  render() {
    const { posts, currentPost, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-6">
          <h4>Post List</h4>
          <ul className="list-group">
            {posts &&
              posts.map((post, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActivePost(post, index)}
                  key={index}
                >
                  {post.title}
                </li>
              ))}
          </ul>
        </div>
        <div className="col-md-6">
          {currentPost ? (
            <div>
              <h4>Post</h4>
              <div>
                <label>
                  <strong>Title:</strong>
                </label>{" "}
                {currentPost.title}
              </div>
              <div>
                <label>
                  <strong>Content:</strong>
                </label>{" "}
                {currentPost.title}
              </div>
              <div>
                <label>
                  <strong>Image:</strong>
                </label>{" "}
                {currentPost.image}
              </div>
              <div>
                <label>
                  <strong>Category:</strong>
                </label>{" "}
                {currentPost.category}
              </div>

              <Link
                to={"/posts/" + currentPost.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
          ) : (
            <div>
              <br />
              <p>Please click on a Post...</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}
